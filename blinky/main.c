#include "main.h"
#include "stm32f4xx.h"

// PA5 <- led
// AHB1
//

int
main(void)
{
  RCC->AHB1ENR |= 0x01;
  GPIOA->MODER |= 0x01 << 10;
  GPIOA->OSPEEDR |= 0x01 << 10;
  uint32_t delay = 10e5;
  uint32_t i = 0;
  while (1) {
    GPIOA->BSRR |= 0x01 << 5;
    for (i = 0; i < delay; i++)
      asm("NOP");
    GPIOA->ODR &= 0x00;
    for (i = 0; i < delay; i++)
      asm("NOP");
  }
}
