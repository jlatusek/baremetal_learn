#include "main.h"
#include "stdio.h"
#include "stm32f4xx.h"

// PA5 <- led
// AHB1
// BTN -> PC13
// UART -> PA2, PA3
// APB1 bit 17 -> enable uart clock

inline void
delay(uint32_t delay)
{
  SysTick->VAL = 0; // initial value for counting
  while (delay > 0) {
    while (!(SysTick->CTRL & 0x10000)) // Check counter reloading
      asm("NOP");
    delay--;
  }
}

void
systic_config()
{
  SysTick->LOAD = 16000 - 1; // Reload systick value
  SysTick->CTRL = 5;         // Enable systick, set clock source from mcu
}

int
main(void)
{
  RCC->AHB1ENR |= 0x01;  // enable PA
  GPIOA->MODER |= 0x400; // enable LED output
  systic_config();

  while (1) {
    GPIOA->ODR ^= 0x20;
    delay(100);
  }
}