#include "main.h"
#include "stdio.h"
#include "stm32f4xx.h"

// PA5 <- led
// AHB1
// BTN -> PC13
// UART -> PA2, PA3
// APB1 bit 17 -> enable uart clock

void
uart_config()
{
  RCC->APB1ENR |= 0x20000; // enable usart2
  RCC->AHB1ENR |= 0x01;    // enable PA
  GPIOA->AFR[0] |= 0x7700; // set altenate function for PA2 and PA3
  GPIOA->MODER |= 0xA0;    // enable UART2 TX and RX
  USART2->BRR = 0x0683;    // 9600 @16MHz
  USART2->CR1 |= 0x000c;   // enable uart tx and rc
  USART2->CR1 |= 0x2000;   // enable uart
}

void
uart_send(int ch)
{
  while (!(USART2->SR & 0x0080))
    ; // wait for empty buffer
  USART2->DR = ch;
}

int
uart_read()
{
  if (USART2->SR & 0x20) {

    return USART2->DR;
  }
  return 0;
}

inline void
delay(uint32_t delay)
{
  for (; delay > 0; delay--) {
    asm("NOP");
  }
}

int
main(void)
{
  RCC->AHB1ENR |= 0x04;   // enable PC
  RCC->AHB1ENR |= 0x01;   // enable PA
  GPIOA->MODER |= 0x400;  // enable LED output
  GPIOC->PUPDR |= 0x2000; // enable pull-up for PC13
  uart_config();
  int buff;

  while (1) {
    GPIOA->ODR ^= 0x20;
    buff = uart_read();
    if (buff != 0) {
      uart_send(buff);
    }
  }
}
struct __FILE
{
  int handle;
};
FILE __stdin = { 0 };
FILE __stdout = { 1 };
FILE __stderr = { 2 };

int
fgetc(FILE* f)
{
  int c;
  c = uart_read();
  if (c == '\r') {
    c = '\n';
  }
  uart_write(c);
  return c;
}

int
fputc(int c, FILE* f)
{
  return uart_write(c);
}