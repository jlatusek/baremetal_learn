#include "main.h"

#include "stdio.h"
#include "stm32f4xx.h"

// PA5 <- led
// AHB1
// TIMER2
//

inline void delay(uint32_t delay);
void timer_config();
void systick_config();
void gpio_config();

int main(void)
{
  systick_config();
  timer_config();

  while (1)
  {
    // GPIOA->ODR ^= 0x20;
    delay(100);
  }
}

void delay(uint32_t delay)
{
  SysTick->VAL = 0; // initial value for counting
  while (delay > 0)
  {
    asm("NOP");
  }
}

void timer_config()
{
  gpio_config();
  RCC->APB1ENR |= 0x01;  // enable clock for timer2
  TIM2->PSC = 1600 - 1;  // divide source clock 16e6 by 16e2
  TIM2->ARR = 10000 - 1; // Set autoreload value set after counting to 0
  TIM2->CCMR1 = 0x30;    // set output to toggle on match
  TIM2->CCER |= 1;       // enable ch1 compare mode
  TIM2->CNT = 0;         // Clear counter
  TIM2->CR1 = 0x1;       // Enable TIM2
}

void systick_config()
{
  SysTick->LOAD = 16000 - 1; // Reload systick value
  SysTick->CTRL = 5;         // Enable systick, set clock source from mcu
}

void gpio_config()
{
  RCC->AHB1ENR |= 0x01;      // enable PA
  GPIOA->MODER |= 0x800;     // enable LED output and set to alternate function
  GPIOA->AFR[0] |= 0x100000; // set AF1 for PA5
}