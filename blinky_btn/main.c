#include "main.h"
#include "stm32f4xx.h"

// PA5 <- led
// AHB1
// BTN
// -> PC13

int
main(void)
{
  RCC->AHB1ENR |= 0x01; //enable PA
  RCC->AHB1ENR |= 0x04; //enable PC
  GPIOA->MODER |= 0x01 << 10;
  GPIOA->OSPEEDR |= 0x01 << 10;
  GPIOC->PUPDR |= 0x01 << 13;
  while (1) {
    if(GPIOC->IDR == 0x01 << 13)
      GPIOA->ODR |= 0x01 << 5;
    else
      GPIOA->ODR &= 0x00;
  }
}
